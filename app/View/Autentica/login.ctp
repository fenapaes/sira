<div class="row">
	<div class="col-md-4">&nbsp;</div>
	<div class="col-md-4">
		<div class="box box-primary" style="margin-top: 200px;">

			<div class="box-header">
				<h3 class="box-title">Login</h3>
			</div>

			<?php echo $this->Form->create('Usuario'); ?>
			<div class="box-body">
					<?php echo $this->Form->input('email'); ?>
					<?php echo $this->Form->input('senha',['type'=>'password']); ?>
			</div>

			<div class="box-footer">
				<div class="pull-right">
					<input class="btn btn-primary" type="submit" value="Login">
				</div>
			</div>

			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

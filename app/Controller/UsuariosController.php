<?php
class UsuariosController extends AppController {

	public function login() {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			if ( $this->Auth->login() ){
				return $this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Session->setFlash('Erro! Usuário ou Senha inválida!', 'flash/errorMessage');
			}
		}
	}

	public function logout() {
		$this->Auth->logout();
		$this->redirect('/');
	}

}

<div class="box box-warning">
	<div class="box-header with-border">
		<span class="box-title">
			Área Social
		</span>
		<div class="box-tools">
			<div class="btn-group">
				<a href="/saude" class="btn btn-sm btn-default">Saúde</a>
				<a href="/educacao" class="btn btn-sm btn-default">Educação</a>
				<a href="#" class="disabled btn btn-sm btn-warning">Social</a>
				<a href="/trabalho" class="btn btn-sm btn-default">Trabalho</a>
				<a href="#" class="disabled btn btn-sm btn-default">Admin</a>
			</div>
		</div>
	</div>
	<div class="box-body">
		<div class="btn-group">
			<a href="/social" class="btn btn-default">Home</a>
			<a href="/social/assistentes" class="btn btn-default">Assistentes</a>
			<a href="/social/assistidos" class="btn btn-default">Assistidos</a>
		</div>
		<div class="btn-group pull-right">
			<a href="#" class="btn btn-default">Mensagens</a>
			<a href="#" class="btn btn-default">Tabelas</a>
			<a href="#" class="btn btn-default">Sair</a>
		</div>
	</div>
</div>

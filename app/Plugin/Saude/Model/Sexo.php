<?php
App::uses('AppModel', 'Model');

class Sexo extends SaudeAppModel {
	
	public $order = [
		'nome' => 'ASC'
	];
	
	public $hasMany = [
		'Medico' => [
			'className' => 'Saude.Medico'
		]
	];
}

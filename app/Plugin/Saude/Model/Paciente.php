<?php
App::uses('AppModel', 'Model');

class Paciente extends SaudeAppModel {

	public $belongsTo = [
		'Sexo' => [
			'className' => 'Saude.Sexo'
		]
	];

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	public function afterFind($results, $primary = false) {
		foreach ($results as $key=>$value) {
			if (isset($value['Paciente']['nascimento'])) {
				$nascimento = date_create_from_format('Y-m-d', $value['Paciente']['nascimento']);
				$results[$key]['Paciente']['nascimento_datebr'] = date_format($nascimento, 'd/m/Y');
			}
		}
		return $results;
	}
}

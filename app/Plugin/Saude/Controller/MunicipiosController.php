<?php
App::uses('AppController', 'Controller');
/**
 * Municipios Controller
 */
class MunicipiosController extends SaudeAppController {
	
	// Paginação padrão para os Controllers
	public $components = array('Paginator');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Post.title' => 'asc'
        )
    );
    
    public $helpers = array('Boootstrap3.BootstrapPaginator');


	// Listar Registros
	public function index() {
		
		$data = $this->Paginator->paginate('Municipio');
		
		$this->set('data', $data);
		
	}
	
	// Adicionar Registro
	public function adicionar() {
		
		$this->render('form');
	}
	
	// Editar Registro
	public function editar($id = null) {

		$this->render('form');
	}
	
	// Visualiar Registro	
	public function ver() {
		
		$this->render('view');
	}
	
	// Excluir Registro
	public function excluir() {
		
	}

}

<?php
App::uses('AppController', 'Controller');
/**
 * Municipios Controller
 */
class MedicosController extends SaudeAppController {
	
	// Paginação padrão para os Controllers
	public $components = array('Paginator');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'Medico.nome' => 'asc'
		)
	);
	public $helpers = array(
		 'Bootstrap3.BootstrapPaginator'
	);
	
	public function beforeFilter() {
		
		 if ($data = $this->request->data) {
			if (isset($data['Search'])) {
				$this->Session->write('Search', $data['Search']);
			}
		}
		parent::beforeFilter();
	}
	
	public function search($clearSearch = false) {
		if ($clearSearch) {
			$this->Session->delete('Search');
			return [];
		}
		if ($this->Session->check('Search')) {
			$search = $this->Session->read('Search');
			$conditions = [];
			if ($search['nome']) {
				array_push($conditions, ['Medico.nome like'=>'%'.$search['nome'].'%']);
			}
			if ($search['sexo_id'] && $search['sexo_id'] != 0) {
				array_push($conditions, ['Medico.sexo_id'=>$search['sexo_id']]);
			}
			if ($search['cpf']) {
				array_push($conditions, ['Medico.cpf like'=>'%'.$search['cpf'].'%']);
			}
			$this->request->data['Search'] = $search;
			
			return $conditions;
			
		} else {
			return [];
		}
	}


	// Listar Registros
	public function index($clearSearch = false) {
		$conditions = $this->search($clearSearch);
		
		$this->Paginator->settings = $this->paginate;
		$data = $this->Paginator->paginate('Medico', $conditions);
		
		$this->set('sexos', ['0'=>'Todos'] + $this->sexoList());
		
		$this->set('data', $data);
		
	}
	
	// Adicionar Registro
	public function adicionar() {
		
		$this->render('form');
	}
	
	// Editar Registro
	public function editar($id = null) {
		
		$this->set('sexos', $this->sexoList());
		
		$data = $this->Medico->read(null, $id);
		
		$this->request->data = $data;

		$this->render('form');
	}
	
	// Visualiar Registro	
	public function ver() {
		
		$this->render('view');
	}
	
	// Excluir Registro
	public function excluir() {
		
	}
	
	public function sexoList() {
		return $this->Medico->Sexo->find('list', ['fields'=>['id','nome']]);
	}

}

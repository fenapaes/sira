<div class="municipios index">
	<div class="row">
		<div class="col-md-3">
			<?php echo $this->Element('page-header', array('title'=>'Municípios','small'=>'Listagem')); ?>
		</div>
		<div class="col-md-9 clearfix">
			<?php echo $this->Element('actions'); ?>
		</div>
	</div>
	
	<div class="box box-primary">
		
	<table class="table table-striped table-hover table-condensed">
		<thead>
			<tr class="active">
				<th class="col-md-1">&nbsp;</th>
				<th><?php echo $this->Paginator->sort('nome'); ?></th>
				<th><?php echo $this->Paginator->sort('estado_id'); ?></th>
					
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $municipio): ?>
			<tr>
				<td>
					<div class="checkbox"><label><input type="checkbox" value="<?php echo $municipio['Municipio']['id'];?>"><?php echo $municipio['Municipio']['id'];?></label></div>
				</td>
				<td><?php echo h($municipio['Municipio']['nome']); ?>&nbsp;</td>
				<td><?php echo h($municipio['Estado']['sigla']); ?></td>
				
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	</div>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('Anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('Próxima') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

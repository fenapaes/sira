<div class="municipios view">
<h2><?php echo __('Municipio'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($municipio['Municipio']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nome'); ?></dt>
		<dd>
			<?php echo h($municipio['Municipio']['nome']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Estado'); ?></dt>
		<dd>
			<?php echo $this->Html->link($municipio['Estado']['id'], array('controller' => 'estados', 'action' => 'view', $municipio['Estado']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Municipio'), array('action' => 'edit', $municipio['Municipio']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Municipio'), array('action' => 'delete', $municipio['Municipio']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $municipio['Municipio']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Municipios'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Municipio'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Estados'), array('controller' => 'estados', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estado'), array('controller' => 'estados', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Associadas'), array('controller' => 'associadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Associada'), array('controller' => 'associadas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Associadas'); ?></h3>
	<?php if (!empty($municipio['Associada'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nome'); ?></th>
		<th><?php echo __('Municipio Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($municipio['Associada'] as $associada): ?>
		<tr>
			<td><?php echo $associada['id']; ?></td>
			<td><?php echo $associada['nome']; ?></td>
			<td><?php echo $associada['municipio_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'associadas', 'action' => 'view', $associada['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'associadas', 'action' => 'edit', $associada['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'associadas', 'action' => 'delete', $associada['id']), array('confirm' => __('Are you sure you want to delete # %s?', $associada['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Associada'), array('controller' => 'associadas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

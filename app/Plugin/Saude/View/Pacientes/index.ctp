
<div class="row">
	<div class="col-md-10">
		<div class="box box-primary">
			<table style="margin: 0 !important;" class="table table-hover">
				<thead>
					<tr class="active">
						<th class="col-md-2">&nbsp;</th>
						<th><?php echo $this->Paginator->sort('nome','Nome do Paciente'); ?></th>
						<th class="col-md-3"><?php echo $this->Paginator->sort('cpf','CPF'); ?></th>
						<th class="col-md-2"><?php echo $this->Paginator->sort('rg','RG'); ?></th>
						<th class="col-md-1"><?php echo $this->Paginator->sort('nascimento','Nascimento'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($data as $paciente) { ?>
					<tr>
						<td><?php echo $this->Element('actions',array('size'=>'2','data'=>array('id'=>$paciente['Paciente']['id']))); ?></td>
						<td><?php echo h($paciente['Paciente']['nome']); ?>&nbsp;</td>
						<td><?php echo h($paciente['Paciente']['cpf']); ?></td>
						<td><?php echo h($paciente['Paciente']['rg']); ?></td>
						<td><?php echo h($paciente['Paciente']['nascimento_datebr']); ?></td>
					</tr>
				<?php } ?>
					<tr class="active">
						<td colspan="5">
							<div class="btn-group pull-right">
								<a href="#" class="btn btn-default">Imprimir</a>
								<a href="#" class="btn btn-primary">Adicionar</a>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="box-footer">
				<?php echo $this->BootstrapPaginator->numbers(array('size'=>'small')); ?>
				<a class="btn btn-primary disabled pull-right">
					<?php echo $this->Paginator->counter(array(
						'format' => 'Página <strong>{:page}</strong> de <strong>{:pages}</strong> de um total de <strong>{:count}</strong> registros.'
					)); ?>
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="box box-primary box-solid">
			<div class="box-header">
				<span class="box-title">Pesquise</span>
			</div>
			<?php echo $this->Form->create('Search'); ?>

			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo $this->Form->input('sexo_id', ['class'=>'form-control']); ?>
					</div>
					<div class="col-md-12">
						<?php echo $this->Form->input('nome', ['class'=>'form-control']); ?>
					</div>
					<div class="col-md-12">
						<?php echo $this->Form->input('cpf', ['class'=>'form-control']); ?>
					</div>
			</div>

			<div class="box-footer">
				<input type="submit" value="Pesquisar..." class="btn btn-default">
				<a href="/saude/medicos/index/1" class="btn btn-default">Limpar</a>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

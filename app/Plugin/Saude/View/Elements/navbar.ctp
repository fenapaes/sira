<div class="box box-primary">
	<div class="box-header with-border">
		<span class="box-title">
			Área de Saúde
		</span>
		<div class="box-tools">
			<div class="btn-group">
				<a href="#" class="disabled btn btn-sm btn-primary">Saúde</a>
				<a href="/educacao" class="btn btn-sm btn-default">Educação</a>
				<a href="/social" class="btn btn-sm btn-default">Social</a>
				<a href="/trabalho" class="btn btn-sm btn-default">Trabalho</a>
				<a href="/admin" class="btn btn-sm btn-default">Admin</a>
			</div>
		</div>
	</div>
	<div class="box-body">
		<div class="btn-group">
			<a href="/saude" class="btn btn-default">Home</a>
			<a href="/saude/medicos" class="btn btn-default">Médicos</a>
			<a href="/saude/pacientes" class="btn btn-default">Pacientes</a>
			<a href="/saude/consultas" class="btn btn-default">Consultas</a>
		</div>
		<div class="btn-group pull-right">
			<a href="#" class="btn btn-default">Mensagens</a>
			<a href="#" class="btn btn-default">Tabelas</a>
			<a href="#" class="btn btn-default">Sair</a>
		</div>
	</div>
</div>

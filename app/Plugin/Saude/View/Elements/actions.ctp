<?php if(!isset($size)) $size = 2; ?>
<div class="btn-group pull-right">
	<?php echo $this->Html->link('<i class="fa fa-<?php echo $size;?>x fa-eye"></i>', array('action'=>'ver',$data['id']), array('escape'=>false, 'class'=>'btn btn-default','title'=>'Ver','data-toggle'=>'tooltip')); ?>
	<?php echo $this->Html->link('<i class="fa fa-<?php echo $size;?>x fa-edit"></i>', array('action'=>'editar',$data['id']), array('escape'=>false, 'class'=>'btn btn-primary','title'=>'Editar','data-toggle'=>'tooltip')); ?>
	
	<?php echo $this->Form->postLink('<i class="fa fa-<?php echo $size;?>x fa-trash"></i>', array('action'=>'excluir',$data['id']), array('escape'=>false, 'class'=>'btn btn-danger','title'=>'Excluir','data-toggle'=>'tooltip'), array('Tem Certeza?')); ?>
	
</div>
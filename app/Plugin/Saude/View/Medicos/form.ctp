<div class="box box-primary">
	<?php echo $this->Form->create('Medico',['novalidate'=>'novalidate']); ?>
	<div class="box-body">
		<div class="row">
			<div class="col-md-9">
				<?php echo $this->Form->input('nome', ['class'=>'form-control']); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->input('nascimento', ['class'=>'form-control','label'=>'Nascimento','type'=>'text']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<?php echo $this->Form->input('sexo_id', ['class'=>'form-control']); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->input('cpf', ['class'=>'form-control','label'=>'CPF']); ?>
			</div>
			<div class="col-md-4">
				<?php echo $this->Form->input('rg', ['class'=>'form-control','label'=>'RG']); ?>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="btn-group pull-right">
		<?php echo $this->Form->submit('Gravar', ['class'=>'btn btn-primary date']); ?>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>

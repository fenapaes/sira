<?php
class AssociadasController extends AdminAppController {

	public function load_municipios() {
		$municipios = $this->Associada->Municipio->find('list', [
			'limit' => 20,
			'fields' => [
				'id','nome'
			]
		]);
		$this->set('municipios', $municipios);
	}

	public function index() {
		$associadas = $this->Associada->find('all');

		$this->set('associadas', $associadas);

	}

	public function add() {

		if ($this->request->isPost()) {
			$data = $this->request->data;
			$this->Associada->save($data);
			$this->redirect(['action'=>'index']);
		} else {
			$this->load_municipios();
		}

		$this->render('form');
	}

	public function edit($id = null) {

		$this->render('form');
	}

	private function save() {

	}

}
 ?>

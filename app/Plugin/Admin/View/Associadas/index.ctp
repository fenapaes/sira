<div class="page-header">Associadas</div>
<table class="table table-bordered table-hover">
	<tr>
		<th class="active">Nome</th>
	</tr>
	<?php foreach($associadas as $item) { ?>
		<tr>
			<td><?php echo $item['Associada']['nome']; ?>
		</tr>
	<?php } ?>
</table>
<hr>
<div class="btn-group">
	<?php echo $this->Html->link('Adicionar', ['action'=>'add'],['class'=>'btn btn-primary']); ?>
</div>

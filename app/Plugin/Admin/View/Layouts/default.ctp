<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>Sira</title>
		<?php
			echo $this->Html->meta('icon');
			echo $this->Html->css('/bower_components/font-awesome/css/font-awesome.min.css');
			echo $this->Html->css('/bower_components/bootstrap/dist/css/bootstrap.min.css');
			echo $this->Html->css('/bower_components/AdminLTE/dist/css/AdminLTE.css');
			echo $this->Html->css('/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css');

			echo $this->Html->script('/bower_components/jquery/dist/jquery.min.js');
			echo $this->Html->script('/bower_components/bootstrap/dist/js/bootstrap.min.js');
			echo $this->Html->script('/bower_components/angular/angular.min.js');

			echo $this->Html->script('/bower_components/AdminLTE/dist/js/app.min.js');

			//echo $this->Html->css('dropzone');
			//echo $this->Html->script('dropzone');

			echo $this->fetch('meta');
			echo $this->fetch('css');
		?>
		<style>
			body {
				padding-top: 10px;
				background:linear-gradient(-45deg, #CCC 10%, #ccc 90%); /* W3C */
			}
			.pagination {
				margin: 0 !important;
			}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<header class="main-header">
				<?php echo $this->Element('Admin.navbar'); ?>
			</header>
			<div class="box box-default">

				<div class="box-body">

				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
				</div>
			</div>
		</div>
		<?php echo $this->fetch('script'); ?>
		<?php echo $this->element('sql_dump'); ?>
	</body>
</html>

<?php
App::uses('AppModel', 'Model');
/**
 * Municipio Model
 *
 * @property Estado $Estado
 * @property Associada $Associada
 */
class Associada extends AdminAppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Municipio' => array(
			'className' => 'Admin.Estado',
			'foreignKey' => 'municipio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


}

<div class="box box-danger">
	<div class="box-header with-border">
		<span class="box-title">
			Área de Trabalho
		</span>
		<div class="box-tools">
			<div class="btn-group">
				<a href="/saude" class="btn btn-sm btn-default">Saúde</a>
				<a href="/educacao" class="btn btn-sm btn-default">Educação</a>
				<a href="/social" class="btn btn-sm btn-default">Social</a>
				<a href="#" class="disabled btn btn-sm btn-danger">Trabalho</a>
				<a href="/admin" class="btn btn-sm btn-default">Admin</a>
			</div>
		</div>
	</div>
	<div class="box-body">
		<div class="btn-group">
			<a href="/trabalho" class="btn btn-default">Home</a>
			<a href="/trabalho/profissionais" class="btn btn-default">Profissionais</a>
			<a href="/trabalho/trabalhadores" class="btn btn-default">Trabalhadores</a>
		</div>
		<div class="btn-group pull-right">
			<a href="#" class="btn btn-default">Mensagens</a>
			<a href="#" class="btn btn-default">Tabelas</a>
			<a href="#" class="btn btn-default">Sair</a>
		</div>
	</div>
</div>

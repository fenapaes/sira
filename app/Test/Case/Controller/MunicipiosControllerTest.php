<?php
App::uses('MunicipiosController', 'Controller');

/**
 * MunicipiosController Test Case
 */
class MunicipiosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.municipio',
		'app.estado',
		'app.associada'
	);

}
